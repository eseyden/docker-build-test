#!/bin/bash
REGISTRY_SERVER=${REGISTRY_SERVER:-172.17.0.1:5000}
[[ !  -z  GIT_COMMIT  ]] && IMAGE_TAG="${GIT_COMMIT:0:7}"
[[ !  -z  $IMAGE_TAG  ]] && IMAGE_TAG=":$IMAGE_TAG"
env
echo "BUILD AND PUSHING $IMAGE_TAG"
sudo docker-compose -f docker-compose-build.yml build
sudo docker tag demo_app $REGISTRY_SERVER/sait/demo_app$IMAGE_TAG
sudo docker tag demo_app_php $REGISTRY_SERVER/sait/demo_app_php$IMAGE_TAG
sudo docker tag demo_app_nginx $REGISTRY_SERVER/sait/demo_app_nginx$IMAGE_TAG
sudo docker push $REGISTRY_SERVER/sait/demo_app$IMAGE_TAG
sudo docker push $REGISTRY_SERVER/sait/demo_app_php$IMAGE_TAG
sudo docker push $REGISTRY_SERVER/sait/demo_app_nginx$IMAGE_TAG
unset REGISTRY_SERVER