#!/bin/bash
docker-compose -f docker-compose-build.yml stop
docker-compose -f docker-compose-build.yml rm
docker-compose -f docker-compose-build.yml build
docker-compose -f docker-compose-build.yml up